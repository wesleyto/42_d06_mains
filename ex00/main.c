/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/15 11:45:44 by wto               #+#    #+#             */
/*   Updated: 2017/08/15 11:54:57 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>

void	ft_putchar(char c);
void	ft_putstr(char *str);
int		ft_strcmp(char *s1, char *s2);
int		ft_strlen(char *str);
void	ft_swap(int *a, int *b);

int		main(void)
{
	char ptverif[] = "If you see this sentence and an X above, ft_putstr and ft_putchar work";
	char st[] = "\"Stringy String\"";
	int a;
	int b;
	int ao;
	int bo;
	char *res_str;

	a = 5;
	b = 3;
	ao = a;
	bo = b;

	ft_putchar('X');
	printf("%s", "\n");
	ft_putstr(ptverif);
	printf("\n%d %d %d (exp: 1, 0, -1)\n", ft_strcmp("a", "A"), ft_strcmp("A", "A"), ft_strcmp("A", "a"));
	printf("%s is reported %d and is actually %lu\n", st, ft_strlen(st), strlen(st));
	ft_swap(&a, &b);
	res_str = a == bo && b == ao ? "Success!" : "Failure!";
	printf("Swapped? %s a=%d (was %d) b=%d (was %d)\n", res_str, a, ao, b, bo);
}















