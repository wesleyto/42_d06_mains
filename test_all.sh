echo ====================
echo ==== Norminette ====
echo ====================

for num in $(seq -f "%02g" 0 4)
	do
		norminette -R CheckForbiddenSourceHeader ex$num/ft*.c
	done


echo
echo ====================
echo ==== Compiling =====
echo ====================

cd ex00
sh libft_creator.sh
cp libft.a ../libft.a
gcc -L.. -lft main.c
cd ..

for num in $(seq -f "%02g" 1 4)
	do
		gcc -Wall -Wextra -Werror -L. -lft -o ex$num/a.out ex$num/ft*.c
	done


echo
echo ====================
echo === Testing Ex00 ===
echo ====================
	ex00/a.out

echo
echo ====================
echo === Testing Ex01 ===
echo ====================
	cd ex01
	./a.out | cat -e
	cd ..
	echo "./a.out" | cat -e

for num in $(seq -f "%02g" 2 4)
	do
		echo
		echo ====================
		echo === Testing Ex$num ===
		echo ====================
		echo ARGS: something-1st-print, another, notfirst, aardvark-2nd-sorted, zebra-last-sorted, 123-1st-sorted, animal-1st-reverse
		echo
		./ex$num/a.out something-1st-print another notfirst aardvark-2nd-sorted zebra-last-sorted 123-1st-sorted animal-1st-reverse
	done